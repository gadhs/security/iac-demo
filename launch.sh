#!/bin/bash

## Initialization script

aws ec2 run-instances \
--region us-east-1 \
--image-id ami-09a41e26df464c548 \
--instance-type t2.micro \
--security-groups inbound-ssh \
--key-name ed25519 \
--user-data file://userdata.sh
