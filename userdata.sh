#!/bin/sh

mkdir -p /srv/salt /srv/pillar
apt install -y curl
curl -L -o /tmp/bootstrap_salt.sh https://bootstrap.saltstack.com
/bin/sh /tmp/bootstrap_salt.sh -i iac-demo -x python3 -X -U
echo "file_client: local" > /etc/salt/minion.d/file_client.conf
salt-call --local pkg.install nmap
reboot
